# Welcome !
This example demonstrates how to create a [SSE](https://en.wikipedia.org/wiki/Server-sent_events) server with [cpprestsdk](https://github.com/Microsoft/cpprestsdk).

# Remote Debug via GDB/gdbserver
The project features a [docker-compose.yml](https://gitlab.com/Tim-S/cpprestserversendeventsserver-example/blob/master/docker-compose.yml) 
that can be used to
[Remote Debug and Develop in CLion](https://www.jetbrains.com/help/clion/remote-projects-support.html).

Simply start the service via
```bash
docker-compose up -d
```
and connect to the container using credentials ssh://debugger@localhost:7776 and password _"pwd"_

## Build And Run the Demo Server without IDE 
You can build and run the executable without the IDE by running the follwoing command once the gdbserver service is up:
```bash
docker-compose exec gdbserver bash 
mkdir build && cd build
cmake ..
make
./CppRestServerSendEventsServer
```

The CppRestServerSendEventsServer should be accessible on [http://localhost:1337](http://localhost:1337).

Check [index.html](./index.html) for an example how to subscribe to the CppRestServerSendEventsServer.
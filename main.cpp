#include <iostream>
#include <thread>
#include "cpprest/uri.h"
#include "cpprest/producerconsumerstream.h"
#include "cpprest/http_listener.h"

using namespace std;
using namespace web;
using namespace http;
using namespace utility;
using namespace concurrency;
using namespace http::experimental::listener;

struct ServerSentEventsServer
{
    explicit ServerSentEventsServer(const string_t& url);
    pplx::task<void> open()  { return mListener.open(); };
    pplx::task<void> close() { return mListener.close(); };
    void publish(const json::value& jsonValue);
    void publish(const char* data, unsigned count);
private:

    void handleGet(const http_request& iRequest);
    http_listener mListener;
    vector<streams::producer_consumer_buffer<char>> mClientBuffers;
    std::mutex m_ClientBuffersLock;
};

ServerSentEventsServer::ServerSentEventsServer(const utility::string_t& url) : mListener(url)
{
    mListener.support(methods::GET, bind(&ServerSentEventsServer::handleGet, this, placeholders::_1));
}

void ServerSentEventsServer::handleGet(const http_request& iRequest)
{
    ucout << iRequest.to_string() << endl;

    http_response wResponse;

    // Setting headers
    wResponse.set_status_code(status_codes::OK);
    wResponse.headers().add(header_names::access_control_allow_origin, U("*"));
    wResponse.headers().add(header_names::content_type, U("text/event-stream; charset=utf-8"));
    wResponse.headers().add(header_names::transfer_encoding, U("chunked"));

    // Preparing buffer
    streams::producer_consumer_buffer<char> wBuffer;
    streams::basic_istream<uint8_t> wStream(wBuffer);
    wResponse.set_body(wStream, U("text/plain; charset=utf-8"));

    auto wReplyTask = iRequest.reply(wResponse);

    //Register new sse-client buffer
    m_ClientBuffersLock.lock();
    mClientBuffers.push_back(wBuffer);
    m_ClientBuffersLock.unlock();

    wReplyTask.wait();// blocking!
    wBuffer.close(std::ios_base::out).wait();// closes the connection
}

void ServerSentEventsServer::publish(const json::value& jsonValue) {
    auto dataString = string("data: ") + jsonValue.serialize();
    publish(dataString.data(), dataString.length());
}

void ServerSentEventsServer::publish(const char * data, unsigned count) {
    m_ClientBuffersLock.lock();
    for (auto wBuffer: mClientBuffers){
        if(wBuffer.is_open()){
            wBuffer.putn_nocopy(data, count).wait();
            wBuffer.putn_nocopy("\n\n", 2).wait();// 1st \n to denote that "data:" is complete; 2nd \n to finalize (http-)chunk
            wBuffer.sync();//.wait();
        }
    }
    m_ClientBuffersLock.unlock();
}

unique_ptr<ServerSentEventsServer> gHttp;

void onInit(const string_t& iAddress)
{
    uri_builder wUri(iAddress);
    auto wAddress = wUri.to_uri().to_string();
    gHttp = std::make_unique<ServerSentEventsServer>(wAddress);

    gHttp->open().wait();
    ucout << string_t(U("Listening for requests at: ")) << wAddress << endl;
}

void onShutdown()
{
    gHttp->close().wait();
}

int main(int argc, char *argv[]) {
    string_t listenUrl = U("http://0.0.0.0:8888");
    if(argc == 2)
        listenUrl = argv[1];

    onInit(listenUrl);

    cout << "Wait until connection occurs..." << endl;

    json::value jsonObject = json::value::object();

    auto stop = false;
    auto testPublisher = new std::thread([&stop, &jsonObject](){
        for (int i = 0;!stop; ++i) {
            jsonObject[U("id")] = json::value("Vehicle #"+to_string(i));
            gHttp->publish(jsonObject);
            this_thread::sleep_for(chrono::seconds(1));
        }
    });

    cout << "Press any key to stop server." << endl;
    getchar();
    stop = true;
    testPublisher->join();
    onShutdown();
    return 0;
}